terraform {
  required_providers {
    cloudflare = {
      source = "cloudflare/cloudflare"
      version = "~> 3.0"
    }
  }
  backend "s3" {
    # encrypt = true
  }
}

variable "records" {
  description = "CloudFlare DNS domain records"
  type = map(any)
}

resource "cloudflare_record" "records" {
  for_each        = var.records
  name            = each.key
  value           = each.value.address
  zone_id         = data.cloudflare_zone.zone.id
  ttl             = try(each.value.ttl, 1) 
  type            = try(each.value.type, "A")
  allow_overwrite = try(var.allow_overwrite, false)
  proxied         = try(each.value.proxy, false) 
  priority        = try(each.value.priority, null)
}

provider "cloudflare" {
  api_token  = var.cloudflare_api_token
}

variable "cloudflare_api_token" {
  description = "CloudFlare API token"
} 

variable "cloudflare_zone_name" {
  description = "CloudFlare Zone Name"
  type        = string
  default     = null
}

variable "allow_overwrite" {
  description = "Allow overwrite existing CloudFlare records"
  type        = bool
  default     = false
}

variable "cloudflare_account_id" {
  description = "Cloudflare Account ID"
  type = string
}

resource "cloudflare_zone" "zone" {
  account_id = var.cloudflare_account_id
  zone       = var.cloudflare_zone_name
  jump_start = true
}

data "cloudflare_zone" "zone" {
  name    = var.cloudflare_zone_name
  depends_on = [
    cloudflare_zone.zone
  ]
}

resource "cloudflare_zone_settings_override" "settings" {
  zone_id = data.cloudflare_zone.zone.id
  settings {
    always_use_https          = "on"
    automatic_https_rewrites  = "on"
    browser_check             = "on"
    min_tls_version           = "1.2"
    privacy_pass             = "off"
    security_header {
      enabled            = true
      include_subdomains = true
      max_age            = 0
      nosniff            = true
      preload            = true
    }
    security_level      = "high"
    ssl                 = "full"
  }
}
